package com.browsercontrol.browserservice;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class BrowserEndpoint {

    public static WebDriver chromeDriver;
    public static WebDriver firefoxDriver;
    private static final String CHROME = "chrome";
    private static final String FIREFOX = "firefox";
    private Connection connection;

    private static final String CHROME_DB = "C:/Users/RK/AppData/Local/Google/Chrome/User Data/Profile 1/History";

    private static Connection getHistoryDBConnection(String sqlitePath) throws ClassNotFoundException {
        Connection connection = null;
        try {
            FileCopyUtils.copy(new File(CHROME_DB), new File("history_back.db"));
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + sqlitePath);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return connection;
    }

    private static WebDriver getDriverByBrowser(String browser) {
        if (browser.equals(CHROME)) {
            if (chromeDriver == null)
                chromeDriver = new ChromeDriver();

            return chromeDriver;
        } else if (browser.equals(FIREFOX)) {
            if (firefoxDriver == null)
                firefoxDriver = new FirefoxDriver();

            return firefoxDriver;
        } else return null;
    }

    @RequestMapping("/start")
    public String startBrowser(@RequestParam("browser") String browser, @RequestParam("url") String url) {
        WebDriver webDriver = getDriverByBrowser(browser);
        webDriver.get(url);
        return url;
    }
    @RequestMapping("/deleteAllHistory")
    public String deleteAllHistory(@RequestParam("browser") String browser)
    {
        try {
            Connection historyDBConnection = getHistoryDBConnection(CHROME_DB);
            PreparedStatement delete_from_visits = historyDBConnection.prepareStatement("DELETE FROM visits");
            delete_from_visits.executeUpdate();
            List<String> history = new ArrayList<>();
            //historyDBConnection.commit();
            //FileCopyUtils.copy(new File("history_back.db"), new File(CHROME_DB));
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping("/getHistory")
    public List<String> getHistory(@RequestParam("browser") String browser) {
        try {
            Connection historyDBConnection = getHistoryDBConnection(CHROME_DB);
            ResultSet resultSet = historyDBConnection.prepareStatement("SELECT  urls_table.title as title FROM visits INNER JOIN\n" +
                    "urls urls_table ON visits.url = urls_table.id\n" +
                    "ORDER BY visits.visit_time DESC").executeQuery();
            List<String> history = new ArrayList<>();
            while (resultSet.next()) {
                history.add(resultSet.getString("title"));
            }
            return history;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping("/getScreenshot")
    public ResponseEntity<Resource> stopBrowser(@RequestParam("browser") String browser) throws FileNotFoundException {
        WebDriver webDriver = getDriverByBrowser(browser);
        TakesScreenshot takesScreenshot = (TakesScreenshot) webDriver;
        File screenshotAs = takesScreenshot.getScreenshotAs(OutputType.FILE);
        File destination = new File("screenshotTake.jpg");
        InputStreamResource resource = new InputStreamResource(new FileInputStream(screenshotAs));

        return ResponseEntity.ok()
                .contentLength(screenshotAs.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }
}
